const jsonObjectConverters =
{
    'datetime': v =>
    {
        return new Date(v[1])
    }
}

function fromJsonClass(obj)
{
    if (obj != null && typeof obj == 'object')
    {
        let jc = obj.__jsonclass__;
        if (typeof jc == 'object' && jc != null && jc instanceof Array) 
        {
            let converter = jsonObjectConverters[jc[0]];
            if (converter != null) return converter(jc);
        }
        else if (obj instanceof Array)
        {
            for (let i=0; obj.length>i; i++)
                obj[i] = fromJsonClass(obj[i]);
        }
        else
        {
            for (let k in obj)
            {
                obj[k] = fromJsonClass(obj[k]);
            }
        }
    }
    return obj;
}

module.exports = { jsonObjectConverters, fromJsonClass };
