const tracjsonrpc = require('trac-jsonrpc-client');
const { fromJsonClass } = require('./jsonrpc-converters');
const queue = require('queue');

class Trac
{
    constructor(url, username, password, maxConcurrentRequests = 1)
    {
        this._client = tracjsonrpc(url, { auth: { username: username, password: password } });
        this._queue = queue({ concurrency: maxConcurrentRequests, autostart: true })
    }

    /** Call RPC method, returning a promise */
    call(method, ...args)
    {
        return new Promise((resolve, reject) =>
        {
            this._queue.push(cb =>
            {
                console.log("start: %s (%s)", method, args.join(", "));
                this._client.callRpc(method, args, (e, r) =>
                {
                    console.log("end  : %s (%s) -> %s", method, args.join(", "), e ? e : "OK");
                    if (e != null) reject(e);
                    else resolve(r);
                    cb();
                })
            })
        })
    }

    /** Get basic ticket properties */
    async getTicket(id)
    {
        let r = await this.call('ticket.get', id);
        return {
            id: r[0],
            created: fromJsonClass(r[1]),
            modified: fromJsonClass(r[2]),
            attributes: fromJsonClass(r[3])
        }
    }

    /** Get a ticket or tickets, optionally including change logs and child tickets
     * 
     * @param {(string|string[]|QueryBuilder)} id - Ticket id or a list of ticket ids
     * @param {Object} [options] - Options
     * @param {boolean} [options.changeLog=false] - Include changelog
     * @param {boolean} [options.childTickets=false] - Include child tickets
     * @param {number} [options.childLevels=0] - How many child levels to include details for (null=infinite)
     * @param {boolean} [options.failAsId=false] - Fail requests as the ticket id instead of failing the entire request
     * @returns {(Object|Object[])} The ticket or tickets
    */
    async getTicketEx(id, options)
    {        
        if (id instanceof QueryBuilder)
        {
            let tickets = await this.query(id);
            return await this.getTicketEx(tickets, options);
        }
        else if (id instanceof Array)
        {
            return Promise.all(id.map(id => this.getTicketEx(id, options)));
        }
        else
        {
            if (options != null && options.failAsId === true)
            {
                try
                {
                    return await this._getTicketEx(id, options);
                }
                catch (e)
                {                
                    return id;
                }
            }
            else
                return this._getTicketEx(id, options);
        }
    }

    async _getTicketEx(id, options)
    {
        if (options == null) options = {};

        let childTickets = options.childTickets === true;
        let childLevels = options.childLevels === undefined ? 0 : options.childLevels;
        let changeLog = options.changeLog === true;

        try
        {
            // query the main ticket
            let t = await this.getTicket(id);

            // start getting the changelog
            let changeLogPromise = changeLog ? this.getChangeLog(id) : null;

            if (childTickets)
            {
                // get child ticket ids
                t.children = [];
                let childIds = await this.query(buildQuery().is('parents', id));

                // recurse?
                if (childLevels == null || (typeof childLevels == 'number' && childLevels > 0))
                {
                    t.children = await this.getTicketEx(childIds,
                    { 
                        ...options,
                        childLevels: childLevels == null
                            ? null
                            : childLevels - 1
                    }, true);
                }
                else
                    t.children = childIds;
            }

            if (changeLogPromise != null)
            {
                try
                {
                    t.changeLog = await changeLogPromise;
                }
                catch (e)
                {
                    // although this is an error, it is a bit silly to crash everything just because it fails
                }
            }

            return t;
        }
        catch (e)
        {
            throw e;
        }
    }
    
    /** Returns a list of ticket ids matching the query string/object */
    async query(qstr)
    {
        // xx = is
        // !xx = is not 
        // ~xx = contains
        // !~xx = doesn't contain
        // ^xx = begins with
        // $xx = ends with

        if (qstr instanceof QueryBuilder)
            qstr = qstr.toString();
        else if (typeof qstr == 'object' && qstr != null)
        {
            let q = qstr;
            let qa = [];
            for (let k in qstr)
            {
                let v = qstr[k];
                if (v instanceof Array)
                    v.forEach(vv => qa.push(k + "=" + vv));
                else
                    qa.push(k + "=" + v);
            }
            qstr = qa.join("&");
        }

        return await this.call('ticket.query', qstr)
    }

    /** Gets the change log for a ticket */
    async getChangeLog(id)
    {
        let r = await this.call('ticket.changeLog', id);
        return r.map(e =>
        {
            return (e[2] == 'comment') ? {
                time: fromJsonClass(e[0]),
                author: e[1],
                field: e[2],
                id: e[3],
                text: e[4],
                permanent: e[5] == 1
            } : {
                time: fromJsonClass(e[0]),
                author: e[1],
                field: e[2],
                oldValue: e[3],
                newValue: e[4],
                permanent: e[5] == 1
            }
        });
    }
}

class QueryBuilder
{
    constructor()
    {
        this._query = [];
    }

    add(key, op, value)
    {
        if (this._negate === true)
        {
            op = "!" + op;
            this._negate = false;
        }

        if (value instanceof Array)
            value.forEach(v => this._query.push(key + '=' + op + v));
        else 
            this._query.push(key + '=' + op + value);

        return this;
    }

    not()
    {
        this._negate = true;
        return this;
    }

    is(key, value)
    {
        return this.add(key, "", value);
    }

    isnt(key, value)
    {
        this._negate = true;
        return this.is(key, value);
    }

    contains(key, value)
    {
        return this.add(key, "~", value);
    }

    doesntContain(key, value)
    {
        this._negate = true;
        return this.contains(key, value);
    }

    beginsWith(key, value)
    {
        return this.add(key, "^", value);        
    }

    endswith(key, value)
    {
        return this.add(key, "$", value);
    }

    toString()
    {
        return this._query.join("&");
    }
}

function buildQuery()
{
    return new QueryBuilder();
}

module.exports = { Trac, QueryBuilder, buildQuery };
